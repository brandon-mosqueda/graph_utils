En este directorio puedes encontrar las imágenes de los 3 diferentes grafos generados para cada uno de los algoritmos de generación de grafos que implementa la biblioteca.

# Tabla de contenido

[[_TOC_]]

## Malla

### 30 nodos

![30 nodos](grid/30.png)

### 100 nodos

![100 nodos](grid/100.png)

### 500 nodos

![500 nodos](grid/500.png)

## Erdös y Rényi

### 30 nodos

![30 nodos](erdos_renyi/30.png)

### 100 nodos

![100 nodos](erdos_renyi/100.png)

### 500 nodos

![500 nodos](erdos_renyi/500.png)

## Gilbert

### 30 nodos

![30 nodos](gilbert/30.png)

### 100 nodos

![100 nodos](gilbert/100.png)

### 500 nodos

![500 nodos](gilbert/500.png)

## Geográfico simple

### 30 nodos

![30 nodos](geographical/30.png)

### 100 nodos

![100 nodos](geographical/100.png)

### 500 nodos

![500 nodos](geographical/500.png)

## Barabási-Albert

### 30 nodos

![30 nodos](barabasi/30.png)

### 100 nodos

![100 nodos](barabasi/100.png)

### 500 nodos

![500 nodos](barabasi/500.png)

## Dorogovtsev-Mendes

### 30 nodos

![30 nodos](dorogovtsev/30.png)

### 100 nodos

![100 nodos](dorogovtsev/100.png)

### 500 nodos

![500 nodos](dorogovtsev/500.png)
