from graph_utils.Graph import Graph

class DirectedGraph(Graph):
    """This class models a directed graph allowing to add nodes, edges and to
    generate a Graphviz representation.

    Attributes:
        graphviz_edge (str): The edge representation on Graphviz format.
        graphviz_type (str): The graph type on Graphviz format.
    """

    graphviz_edge = "->"
    graphviz_type = "digraph"

    def has_edge(self, from_node, to_node):
        for edge in self.edges:
            if (edge.from_node.name == from_node and
                edge.to_node.name == to_node):
                return True

        return False
