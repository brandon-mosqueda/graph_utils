from setuptools import setup

setup(name='graph_utils',
      version='0.1',
      description='A library to describe and manipulate graphs.',
      author='Brandon Alejandro Mosqueda González',
      author_email='bmosquedag2021@cic.ipn.mx',
      license='MIT',
      zip_safe=False)
