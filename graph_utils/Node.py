class Node:
    """A class that holds the information about a node of a graph.

    Attributes:
        name (str): The node's name.
    """

    def __init__(self, name):
        """Constructor of the class Node.

        Args:
            name (str): The node's name.
        """
        self.name = name
