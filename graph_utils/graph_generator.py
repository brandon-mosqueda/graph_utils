from math import sqrt
from random import randint, random
from graph_utils.Graph import Graph
from graph_utils.DirectedGraph import DirectedGraph

def get_graph_object(is_directed):
    """Get an instance of a Graph or DirectedGraph class.

    Args:
        is_directed (bool): Do you want a directed graph?

    Returns:
        Graph or DirectedGraph: The instance.
    """
    GraphClass = DirectedGraph if is_directed else Graph
    graph = GraphClass()

    return graph


def get_node_name(i):
    """Get a node name for an arbitrary node i.

    Args:
        i (int): The node number.

    Returns:
        str: The node's name.
    """
    return "node_" + str(i)


def get_grid_node_name(i, j):
    """Get a node name for an arbitrary node i, j for the grid algorithm.

    Args:
        i (int): The node i number.
        j (int): The node j number.

    Returns:
        str: The node's name.
    """
    return "node_" + str(i) + "_" + str(j)


def euclidean_distance(x, y):
    """Compute the euclidean distance of two points.

    Args:
        x (tuple or list): The coordinates of the first point.
        y (tuple or list): The coordinates of the second point.

    Returns:
        float: The eucliden distance.
    """
    return sqrt((x[0] - y[0])**2 + (x[1] - y[1])**2)


def generate_grid_graph(m, n, is_directed=False):
    """Generates a graph using the grid algorithm.

    Args:
        m (int): The number of colums.
        n (int): The number of rows.
        is_directed (bool, optional): Is it a directed graph?. Defaults to
            False.

    Raises:
        Exception: When m <= 1 or n <= 1.

    Returns:
        Graph or DirectedGraph: The grid graph.
    """
    if (m <= 1 or n <= 1):
        raise Exception("m and n must be > 1")

    graph = get_graph_object(is_directed)

    for i in range(m):
        for j in range(n):
            graph.add_node(get_grid_node_name(i, j))

    for i in range(m - 1):
        for j in range(n - 1):
            node_i_j = get_grid_node_name(i, j)
            neighbor_node = get_grid_node_name(i + 1, j)
            neighbor_node2 = get_grid_node_name(i, j + 1)
            graph.add_edge(node_i_j, neighbor_node)
            graph.add_edge(node_i_j, neighbor_node2)

    return graph


def generate_erdos_renyi_graph(nodes_num,
                               edges_num,
                               is_directed=False,
                               has_autocycles=False):
    """Generates a graph using the Erdos-Renyi algorithm.

    Args:
        nodes_num (int): The number of nodes.
        edges_num (int): The number of edges.
        is_directed (bool, optional): Is it a directed graph?. Defaults to
            False.
        has_autocycles (bool, optional): Are autocycles allowed?. Defaults to
            False.

    Raises:
        Exception: When nodes_num < 1 or edges_num < nodes_num - 1.

    Returns:
        Graph or DirectedGraph: The Erdos-Renyi graph.
    """
    if (nodes_num < 1):
        raise Exception("nodes_num must be > 0")
    elif (edges_num < nodes_num - 1):
        raise Exception("edges_num must be >= nodes_num - 1")

    graph = get_graph_object(is_directed)

    for i in range(nodes_num):
        graph.add_node(get_node_name(i))

    for i in range(edges_num):
        x = randint(0, nodes_num - 1)
        y = randint(0, nodes_num - 1)

        node_x = get_node_name(x)
        node_y = get_node_name(y)

        if (not has_autocycles and x == y):
            continue

        graph.add_edge(node_x, node_y)

    return graph


def generate_gilbert_graph(nodes_num,
                           probability,
                           is_directed=False,
                           has_autocycles=False):
    """Generates a graph using the Gilbert algorithm.

    Args:
        nodes_num (int): The number of nodes.
        probability (float): The probability of creating an edge.
        is_directed (bool, optional): Is it a directed graph?. Defaults to
            False.
        has_autocycles (bool, optional): Are autocycles allowed?. Defaults to
            False.

    Raises:
        Exception: When nodes_num < 1 or (probability < 0 or probability > 1).

    Returns:
        Graph or DirectedGraph: The Gilbert graph.
    """
    if (nodes_num < 1):
        raise Exception("nodes_num must be > 0")
    elif (probability < 0 or probability > 1):
        raise Exception("probability must be >= 0 and <= 1")

    graph = get_graph_object(is_directed)

    for i in range(nodes_num):
        graph.add_node(get_node_name(i))

    for i in range(nodes_num):
        for j in range(nodes_num):
            if(random() <= probability):
                if (not has_autocycles and i == j):
                    continue

                node_i = get_node_name(i)
                node_j = get_node_name(j)
                graph.add_edge(node_i, node_j)

    return graph


def generate_geographical_graph(nodes_num,
                                max_distance,
                                is_directed=False,
                                has_autocycles=False):
    """Generates a graph using the geographical simple algorithm.

    Args:
        nodes_num (int): The number of nodes.
        max_distance (float): The maximun distance to create an edge.
        is_directed (bool, optional): Is it a directed graph?. Defaults to
            False.
        has_autocycles (bool, optional): Are autocycles allowed?. Defaults to
            False.

    Raises:
        Exception: When nodes_num < 1 or (max_distance <= 0 or max_distance >=
        1).

    Returns:
        Graph or DirectedGraph: The geographical graph.
    """
    if (nodes_num < 1):
        raise Exception("nodes_num must be > 0")
    elif (max_distance <= 0 or max_distance >= 1):
        raise Exception("max_distance must be > 0 and < 1")

    graph = get_graph_object(is_directed)
    coordinates = []

    for i in range(nodes_num):
        x = random()
        y = random()
        coordinates.append((x, y))
        graph.add_node(get_node_name(i))

    for i in range(nodes_num):
        for j in range(nodes_num):
            distance = euclidean_distance(coordinates[i], coordinates[j])

            if (distance <= max_distance):
                if (not has_autocycles and i == j):
                    continue

                node_i = get_node_name(i)
                node_j = get_node_name(j)
                graph.add_edge(node_i, node_j)

    return graph


def generate_barabasi_albert_graph(nodes_num,
                                   max_node_degree,
                                   is_directed=False,
                                   has_autocycles=False):
    """Generates a graph using the Barabási-Albert algorithm.

    Args:
        nodes_num (int): The number of nodes.
        max_node_degree (int): The expected maximum degree for a node.
        is_directed (bool, optional): Is it a directed graph?. Defaults to
            False.
        has_autocycles (bool, optional): Are autocycles allowed?. Defaults to
            False.

    Raises:
        Exception: When nodes_num < 1 or max_node_degree <= 0.

    Returns:
        Graph or DirectedGraph: The Barabási-Albert graph.
    """
    if (nodes_num < 1):
        raise Exception("nodes_num must be > 0")
    elif (max_node_degree <= 0):
        raise Exception("max_node_degree must be > 0")

    graph = get_graph_object(is_directed)
    degrees = [0] * nodes_num

    for i in range(nodes_num):
        graph.add_node(get_node_name(i))

        for j in range(i + 1):
            if (degrees[j] == max_node_degree):
                continue

            probability = 1 - (degrees[j] / max_node_degree)

            if (random() <= probability):
                if (not has_autocycles and i == j):
                    continue

                node_i = get_node_name(i)
                node_j = get_node_name(j)
                graph.add_edge(node_i, node_j)

                degrees[i] += 1
                # Tecnically this should be applied only in directed graphs
                # but if it is not present, almost all nodes will conect with
                # the first nodes because its degree is always 0.
                degrees[j] += 1

    return graph


def generate_dorogovtsev_mendes_graph(nodes_num, is_directed=False):
    """Generates a graph using the Dorogovtsev-Mendes algorithm.

    Args:
        nodes_num (int): The number of nodes.
        is_directed (bool, optional): Is it a directed graph?. Defaults to
            False.

    Raises:
        Exception: When nodes_num < 3.

    Returns:
        Graph or DirectedGraph: The Dorogovtsev-Mendes graph.
    """
    if (nodes_num < 3):
        raise Exception("nodes_num must be >= 3")

    graph = get_graph_object(is_directed)
    node_0 = get_node_name(0)
    node_1 = get_node_name(1)
    node_2 = get_node_name(2)

    graph.add_nodes([node_0, node_1, node_2])
    graph.add_edges([(node_0, node_1), (node_1, node_2), (node_2, node_0)])

    for i in range(3, nodes_num):
        node_i = get_node_name(i)
        graph.add_node(node_i)

        edges_num = len(graph.edges)
        edge = graph.edges[randint(0, edges_num - 1)]
        neighbor_node = edge.from_node.name
        neighbor_node2 = edge.to_node.name

        graph.add_edges([(node_i, neighbor_node), (node_i, neighbor_node2)])

    return graph
