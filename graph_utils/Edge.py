class Edge:
    """A class that holds the information about an edge of a graph, the
    conection of two nodes.

    Attributes:
        from_node (Node): The source node.
        to_node (Node): The destination node.
    """

    def __init__(self, from_node, to_node):
        """The constructor of Edge class.

        Args:
            from_node (Node): The source node.
            to_node (Node): The destination node.
        """
        self.from_node = from_node
        self.to_node = to_node
