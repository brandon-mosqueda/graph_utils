from graph_utils.Node import Node
from graph_utils.Edge import Edge

class Graph:
    """This class models a undirected graph allowing to add nodes, edges and to
    generate a Graphviz representation.

    Attributes:
        nodes (dict): The graph's nodes. Each key holds a Node object.
        edges (list): The graph's edges. A list of Edge objects.
        graphviz_edge (str): The edge representation on Graphviz format.
        graphviz_type (str): The graph type on Graphviz format.
    """

    graphviz_edge = "--"
    graphviz_type = "graph"

    def __init__(self):
        """The Graph constructor."""
        self.nodes = {}
        self.edges = []

    def add_node(self, node_name):
        """Add one node to the graph if it is not included yet.

        Args:
            node_name (str): The node's name.
        """
        if (node_name not in self.nodes):
            node = Node(node_name)
            self.nodes[node_name] = node

    def add_nodes(self, nodes_names):
        """Add several nodes to the graph if they are not included yet.

        Args:
            nodes_names (list): A list of node's names to be added.
        """
        for node_name in nodes_names:
            self.add_node(node_name)

    def has_edge(self, from_node, to_node):
        """Check if the graph contains an edge from one to another.

        Args:
            from_node (str): The source node's name.
            to_node (str): The destination node's name.

        Returns:
            bool: Is it the edge in the graph?
        """
        for edge in self.edges:
            from_name = edge.from_node.name
            to_name = edge.to_node.name
            condition1 = from_name == from_node and to_name == to_node
            condition2 = to_name == from_node and from_name == to_node

            if condition1 or condition2:
                return True

        return False

    def is_valid_edge(self, from_node, to_node):
        """Check if two nodes can be used to form an edge in the graph.

        Args:
            from_node (str): The source node's name.
            to_node (str): The destination node's name.

        Returns:
            bool: Are both nodes in the graph?
        """
        has_from_node = self.nodes.get(from_node) is not None
        has_to_node = self.nodes.get(to_node) is not None

        return has_from_node and has_to_node

    def add_edge(self, from_node, to_node):
        """Add one edge to the graph if it is not included yet and it is valid.

        Args:
            from_node (str): The source node's name.
            to_node (str): The destination node's name.
        """
        has_edge = self.has_edge(from_node, to_node)
        is_valid_edge = self.is_valid_edge(from_node, to_node)

        if (not has_edge and is_valid_edge):
            edge = Edge(self.nodes.get(from_node), self.nodes.get(to_node))
            self.edges.append(edge)

    def add_edges(self, edges):
        """Add several edges to the graph if they are not included yet and they
        are valid.

        Args:
            edges (list): A list with inner tuples or lists of pair of nodes
                names in the form (from_node, to_node).
        """
        for edge in edges:
            self.add_edge(edge[0], edge[1])

    def as_graphviz(self):
        """Generate the Graphviz representation of the object.

        Returns:
            str: The Graphviz representation.
        """
        if not self.edges:
            return "%s {}" % self.graphviz_type

        edges_string = ""
        for edge in self.edges:
            edges_string += "\t%s %s %s\n" % (edge.from_node.name,
                                              self.graphviz_edge,
                                              edge.to_node.name)

        return "%s {\n%s}\n" % (self.graphviz_type, edges_string)

    def save_as_graphviz(self, file_name):
        """Save the Graphviz representation in a file.

        Args:
            file_name (str): The file name to store in.
        """
        text = self.as_graphviz()
        textfile = open(file_name, "w")
        textfile.write(text)
        textfile.close()
