import os
from graph_utils.graph_generator import *

os.makedirs("graphs/grid")
os.makedirs("graphs/erdos_renyi")
os.makedirs("graphs/gilbert")
os.makedirs("graphs/geographical")
os.makedirs("graphs/barabasi")
os.makedirs("graphs/dorogovtsev")

# Grid graphs --------------------------------------------------
graph = generate_grid_graph(5, 6)
graph.save_as_graphviz("graphs/grid/30.dot")

graph = generate_grid_graph(10, 10)
graph.save_as_graphviz("graphs/grid/100.dot")

graph = generate_grid_graph(25, 20)
graph.save_as_graphviz("graphs/grid/500.dot")

# Erdös y Rényi graphs--------------------------------------------------
graph = generate_erdos_renyi_graph(30, 60)
graph.save_as_graphviz("graphs/erdos_renyi/30.dot")

graph = generate_erdos_renyi_graph(100, 150)
graph.save_as_graphviz("graphs/erdos_renyi/100.dot")

graph = generate_erdos_renyi_graph(500, 750)
graph.save_as_graphviz("graphs/erdos_renyi/500.dot")

# Gilbert graphs--------------------------------------------------
graph = generate_gilbert_graph(30, 0.8)
graph.save_as_graphviz("graphs/gilbert/30.dot")

graph = generate_gilbert_graph(100, 0.7)
graph.save_as_graphviz("graphs/gilbert/100.dot")

graph = generate_gilbert_graph(500, 0.3)
graph.save_as_graphviz("graphs/gilbert/500.dot")

# Geographical graphs--------------------------------------------------
graph = generate_geographical_graph(30, 0.7)
graph.save_as_graphviz("graphs/geographical/30.dot")

graph = generate_geographical_graph(100, 0.5)
graph.save_as_graphviz("graphs/geographical/100.dot")

graph = generate_geographical_graph(500, 0.2)
graph.save_as_graphviz("graphs/geographical/500.dot")

# Barabasi-Albert graphs--------------------------------------------------
graph = generate_barabasi_albert_graph(30, 4)
graph.save_as_graphviz("graphs/barabasi/30.dot")

graph = generate_barabasi_albert_graph(100, 5)
graph.save_as_graphviz("graphs/barabasi/100.dot")

graph = generate_barabasi_albert_graph(500, 10)
graph.save_as_graphviz("graphs/barabasi/500.dot")

# Dorogovtsev-Mendes graphs--------------------------------------------------
graph = generate_dorogovtsev_mendes_graph(30)
graph.save_as_graphviz("graphs/dorogovtsev/30.dot")

graph = generate_dorogovtsev_mendes_graph(100)
graph.save_as_graphviz("graphs/dorogovtsev/100.dot")

graph = generate_dorogovtsev_mendes_graph(500)
graph.save_as_graphviz("graphs/dorogovtsev/500.dot")
