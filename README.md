# Proyecto: Biblioteca de generación y manejo de grafos

Se trata de una biblioteca orientada a objetos, en Python 3.6, para describir y utilizar grafos.

## Estructura del repositorio

En el directorio raíz se encuentran archivos como el `README.md` y `LICENSE`, así como un archivo que hace uso de la biblioteca desarrollada que sirve como ejemplo de cómo utilizar la librería.

El directorio `graphs` contiene los archivos `.dot` con la definición de los grafos en formato graphviz simple. El directorio `images` contiene las imágenes de los grafos generadas en Gephi.

Finalmente, el directorio `graph_utils` contiene el código fuente de la librería que lleva este mismo nombre.

## Primer entregable: Algoritmos de generación de grafos

Se implementaron los siguientes algoritmos de generación de grafos:

* G_{m,n} de malla.
* G_{n,m} de Erdös y Rényi
* G_{n,p} de Gilbert.
* G_{n,r} geográfico simple.
* G_{n,d} Barabási-Albert.
* G_n Dorogovtsev-Mendes.

Para cada algoritmo se generaron 3 grafos diferentes con 30, 100 y 500 nodos. El archivo `1.sample_graphs.py` incluye el código para generar todos los archivos `.dot` que aparecen en la carpeta `graphs`.
